
locals {
    context_tags = {
        "Terraform" = "true"
        "Cost-Center" = "${var.context.cost_center}"
        "Project-Code" = "${var.context.project_code}"
        "Region" = "${var.context.region}"
        "Environment" = "${var.context.env_name}"
        "Zone" = "${var.context.zone}"
        "Tier" = "${var.context.tier}"
    }
}