module "my_bucket_module" {
  source = "../src"

  # Module Parameters here
  bucket_name = var.bucket_name   
  force_destroy = var.force_destroy
  versioning = var.versioning
  context = var.context
  additional_tags = var.additional_tags
}
