# Publishing Terraform Modules to Gitlab Infra Registry

This repository serves as an example of how to create and publish a Terraform module to the GitLab Terraform Infrastructure Registry. To understand the full process and how it works, please refer to the accompanying article on Medium titled "Publishing Terraform Modules to GitLab Infra Registry" found at the following link: [Publishing Terraform Modules to Gitlab Infra Registry](https://medium.com/p/a52755ebc712)

